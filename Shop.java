import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Scanner;


public class Shop{

   
    public static void main(String[] args) {
 
    	HashMap<String, Double> kosz = new HashMap<String, Double>();
    	Scanner scan = new Scanner(System.in);
	
   		kosz.put("water", 3.5);
        kosz.put("beer", 5.0);
        kosz.put("bread", 3.0);
        kosz.put("car", 10000.0);
   	
        double konto = 1000;
  
     	System.out.println(kosz);   
     	System.out.println("Choose product:");
     	String x = scan.next();
     		
     	if(kosz.containsKey(x))
     		{	
     		double products = kosz.get(x);
     		koszyk(x, konto, products);
            double bank = konto - kosz.get(x);
    
            kup(konto, products);
     
            double acc = bank - kosz.get(x);
     
            repeat(bank);
           
     		}
     	else
     		{
     			  
     		System.out.println("We dont have that product.");
     		repeat(konto);
     		}
     	
     	
    }
    public static void kup(double konto, double a)
    { 
    		
    Date data = new Date();
    SimpleDateFormat hour = new SimpleDateFormat("HH:mm dd/MM/yyyy");
            
    double b = (konto - a);
    
    if (b >=0)
            	
            {
            System.out.println("Account: " + (konto - a) + "z");
            System.out.println("Bought. Odd money: " + (konto-a) + "z");
            System.out.println(hour.format(data));
            }
    else 
            {
            System.out.println("Decline");
            System.out.println(hour.format(data));
            System.exit(0);
            }  
   
             
    }

    public static void repeat (double bank)
    {	
    	
    	
    	System.out.println("Something else? (yes/no)");
    	Scanner scan2 = new Scanner(System.in);
    	String pytanie = scan2.nextLine();
    	
    	if(pytanie.equals("yes"))
    		{
    		HashMap<String, Double> kosz = new HashMap<String, Double>();
        	Scanner scan = new Scanner(System.in);
        	
       		kosz.put("water", 3.5);
            kosz.put("beer", 5.0);
            kosz.put("bread", 3.0);
            kosz.put("car", 10000.0);
            System.out.println("Choose product:");
       	
    	    String x = scan.next();
    	   
    	    if(kosz.containsKey(x))
    			{	
    	    	double products = kosz.get(x);
      		
    	    	koszyk(x, bank, products);
      
    	    	kup(bank, products); 
    	    	double acc = bank - kosz.get(x);
      
    	    	repeat(acc);
    			}
    	    else
    			{
    			
    			System.out.println("We don't have that product.");
    			repeat(bank);
    			}
     	
    		
    		
    		}
    	else if (pytanie.equals("no"))
    		{
    		System.out.println("Thank you and come back again.");
    		
    		scan2.close();
    		
    		
    		}
    	else 
    		{
    		
    		System.out.println("Only answer yes/no.");
    		repeat(bank);
    		}
    	
    	
    }
    public static void koszyk(String x, double konto, double products)
    	{	
    
    	HashMap<String, Double> kosz = new HashMap<String, Double>();
    	
    	kosz.put("water", 3.5);
        kosz.put("beer", 5.0);
        kosz.put("bread", 3.0);
        kosz.put("car", 10000.0);
    	
    	
    	if(kosz.containsKey(x) && (konto - products)>0)
 			{
 		
    		System.out.println("Bought: " + x);
 		
 			}
    	else if ((konto-products)<0)
    		{
 			System.out.println("You can't afford that product.");
    		}
    	else 
 			{
 			
 			System.out.println("We don't have that product.");
 			System.out.println(kosz);
 			} 
 		
    	
    }
    
}
